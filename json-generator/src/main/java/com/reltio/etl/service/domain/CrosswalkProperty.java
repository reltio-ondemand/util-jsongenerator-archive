package com.reltio.etl.service.domain;

import java.io.Serializable;

/**
 * Created by gowganesh on 21/06/16.
 */
public class CrosswalkProperty implements Serializable {


    private String sourceSystem;
    private String crosswalkValueColumn;
    private String sourceTable;
    private boolean isStaticSourceSystem;
    private Integer sourceSystemColumnIndex;
    private Integer crosswalkValueColumnIndex;
    private boolean isNormalized;

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getCrosswalkValueColumn() {
        return crosswalkValueColumn;
    }

    public void setCrosswalkValueColumn(String crosswalkValueColumn) {
        this.crosswalkValueColumn = crosswalkValueColumn;
    }

    public boolean isStaticSourceSystem() {
        return isStaticSourceSystem;
    }

    public void setStaticSourceSystem(boolean staticSourceSystem) {
        isStaticSourceSystem = staticSourceSystem;
    }

    public Integer getSourceSystemColumnIndex() {
        return sourceSystemColumnIndex;
    }

    public void setSourceSystemColumnIndex(Integer sourceSystemColumnIndex) {
        this.sourceSystemColumnIndex = sourceSystemColumnIndex;
    }

    public Integer getCrosswalkValueColumnIndex() {
        return crosswalkValueColumnIndex;
    }

    public void setCrosswalkValueColumnIndex(Integer crosswalkValueColumnIndex) {
        this.crosswalkValueColumnIndex = crosswalkValueColumnIndex;
    }

    public boolean isNormalized() {
        return isNormalized;
    }

    public void setNormalized(boolean normalized) {
        isNormalized = normalized;
    }
}
