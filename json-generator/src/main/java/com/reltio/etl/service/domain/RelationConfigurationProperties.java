package com.reltio.etl.service.domain;

import java.io.Serializable;
import java.util.Properties;
import java.util.regex.Pattern;

import static com.reltio.etl.constants.JsonGeneratorProperties.*;

public class RelationConfigurationProperties implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5287955917717614956L;

    private String inputDataFilePath;
    private String inputFileFormat;
    private String inputFileDelimiter;
    private String outputFilePath;
    private String mappingFilePath;
    private String relationType;

    private String sourceSystem;

    private String startObjSourceSystem;
    private String startObjCrosswalkValueColumn;
    private String startObjSourceTable;

    private String startObjEntityUriColumn;

    private String endObjSourceSystem;
    private String endObjCrosswalkValueColumn;
    private String endObjSourceTable;

    private String endObjEntityUriColumn;

    private Integer threadCount;
    private String columnDelimiter;

    private Boolean isSingleJsonOutput = false;
    private Boolean isJsonWithOutCrosswalk = false;

    private String crosswalkValueColumn;
    private String sourceTable;


    private String startDateColumn;
    private String endDateColumn;

    public RelationConfigurationProperties(Properties properties) {
        // READ the Config Properties values
        inputDataFilePath = properties.getProperty("INPUT_DATA_FILE_PATH");
        inputFileFormat = properties.getProperty("INPUT_FILE_FORMAT");
        inputFileDelimiter = properties.getProperty("INPUT_FILE_DELIMITER");
        outputFilePath = properties.getProperty("OUTPUT_FILE_PATH");
        mappingFilePath = properties.getProperty("MAPPING_FILE_PATH");

        relationType = properties.getProperty("RELATION_TYPE");
        sourceSystem = properties.getProperty("SOURCE_SYSTEM");

        startObjSourceSystem = properties
                .getProperty("START_OBJECT_SOURCE_SYSTEM");
        startObjCrosswalkValueColumn = properties
                .getProperty("START_OBJECT_CROSSWALK_VALUE_COLUMN");
        startObjSourceTable = properties.getProperty("START_OBJECT_SOURCE_TABLE");
        startObjEntityUriColumn = properties
                .getProperty("START_OBJECT_ENTITY_URI_COLUMN");

        endObjSourceSystem = properties.getProperty("END_OBJECT_SOURCE_SYSTEM");
        endObjCrosswalkValueColumn = properties
                .getProperty("END_OBJECT_CROSSWALK_VALUE_COLUMN");
        endObjSourceTable = properties.getProperty("END_OBJECT_SOURCE_TABLE");
        endObjEntityUriColumn = properties
                .getProperty("END_OBJECT_ENTITY_URI_COLUMN");

        if (startObjCrosswalkValueColumn != null
                && startObjSourceSystem == null) {
            startObjSourceSystem = sourceSystem;

        }

        if (endObjCrosswalkValueColumn != null && endObjSourceSystem == null) {
            endObjSourceSystem = sourceSystem;

        }

        String threadCountStr = properties.getProperty("THREAD_COUNT");
        if (threadCountStr == null || threadCountStr.isEmpty()) {
            setThreadCount(null);
        } else {
            setThreadCount(Integer.parseInt(threadCountStr));
        }

        columnDelimiter = properties
                .getProperty("NORMALIZED_FILE_COLUMN_DELIMITER");

        if (columnDelimiter != null && !columnDelimiter.isEmpty()) {
            columnDelimiter = Pattern.quote(columnDelimiter);
        }

        String jsonFormat = properties.getProperty("JSON_OUTPUT_FORMAT");
        if (jsonFormat != null) {
            if (jsonFormat.equalsIgnoreCase(SINGLE_JSON_OUTPUT)) {
                isSingleJsonOutput = true;

            } else if (jsonFormat.equalsIgnoreCase(JSON_WITHOUT_CROSSWALK)) {
                isJsonWithOutCrosswalk = true;

            }
        }

        crosswalkValueColumn = properties
                .getProperty("RELATION_CROSSWALK_VALUE_COLUMN");
        sourceTable = properties.getProperty("SOURCE_TABLE");

        startDateColumn = properties.getProperty("RELATION_START_DATE_COLUMN");

        endDateColumn = properties.getProperty("RELATION_END_DATE_COLUMN");

    }

    /**
     * @return the threadCount
     */
    public Integer getThreadCount() {
        return threadCount;
    }

    /**
     * @param threadCount the threadCount to set
     */
    public void setThreadCount(Integer threadCount) {
        if (threadCount == null) {
            this.threadCount = MIN_THREAD_COUNT;
        } else if (threadCount > MAX_THREAD_COUNT) {
            this.threadCount = MAX_THREAD_COUNT;

        } else {
            this.threadCount = threadCount;
        }
    }

    public String getStartObjSourceTable() {
        return startObjSourceTable;
    }

    public void setStartObjSourceTable(String startObjSourceTable) {
        this.startObjSourceTable = startObjSourceTable;
    }

    public String getEndObjSourceTable() {
        return endObjSourceTable;
    }

    public void setEndObjSourceTable(String endObjSourceTable) {
        this.endObjSourceTable = endObjSourceTable;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

    /**
     * @return the inputDataFilePath
     */
    public String getInputDataFilePath() {
        return inputDataFilePath;
    }

    /**
     * @param inputDataFilePath the inputDataFilePath to set
     */
    public void setInputDataFilePath(String inputDataFilePath) {
        this.inputDataFilePath = inputDataFilePath;
    }

    /**
     * @return the inputFileFormat
     */
    public String getInputFileFormat() {
        return inputFileFormat;
    }

    /**
     * @param inputFileFormat the inputFileFormat to set
     */
    public void setInputFileFormat(String inputFileFormat) {
        this.inputFileFormat = inputFileFormat;
    }

    /**
     * @return the inputFileDelimiter
     */
    public String getInputFileDelimiter() {
        return inputFileDelimiter;
    }

    /**
     * @param inputFileDelimiter the inputFileDelimiter to set
     */
    public void setInputFileDelimiter(String inputFileDelimiter) {
        this.inputFileDelimiter = inputFileDelimiter;
    }

    /**
     * @return the outputFilePath
     */
    public String getOutputFilePath() {
        return outputFilePath;
    }

    /**
     * @param outputFilePath the outputFilePath to set
     */
    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    /**
     * @return the mappingFilePath
     */
    public String getMappingFilePath() {
        return mappingFilePath;
    }

    /**
     * @param mappingFilePath the mappingFilePath to set
     */
    public void setMappingFilePath(String mappingFilePath) {
        this.mappingFilePath = mappingFilePath;
    }

    /**
     * @return the relationType
     */
    public String getRelationType() {
        return relationType;
    }

    /**
     * @param relationType the relationType to set
     */
    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    /**
     * @return the sourceSystem
     */
    public String getSourceSystem() {
        return sourceSystem;
    }

    /**
     * @param sourceSystem the sourceSystem to set
     */
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    /**
     * @return the startObjSourceSystem
     */
    public String getStartObjSourceSystem() {
        return startObjSourceSystem;
    }

    /**
     * @param startObjSourceSystem the startObjSourceSystem to set
     */
    public void setStartObjSourceSystem(String startObjSourceSystem) {
        this.startObjSourceSystem = startObjSourceSystem;
    }

    /**
     * @return the startObjCrosswalkValueColumn
     */
    public String getStartObjCrosswalkValueColumn() {
        return startObjCrosswalkValueColumn;
    }

    /**
     * @param startObjCrosswalkValueColumn the startObjCrosswalkValueColumn to set
     */
    public void setStartObjCrosswalkValueColumn(
            String startObjCrosswalkValueColumn) {
        this.startObjCrosswalkValueColumn = startObjCrosswalkValueColumn;
    }

    /**
     * @return the startObjEntityUriColumn
     */
    public String getStartObjEntityUriColumn() {
        return startObjEntityUriColumn;
    }

    /**
     * @param startObjEntityUriColumn the startObjEntityUriColumn to set
     */
    public void setStartObjEntityUriColumn(String startObjEntityUriColumn) {
        this.startObjEntityUriColumn = startObjEntityUriColumn;
    }

    /**
     * @return the endObjSourceSystem
     */
    public String getEndObjSourceSystem() {
        return endObjSourceSystem;
    }

    /**
     * @param endObjSourceSystem the endObjSourceSystem to set
     */
    public void setEndObjSourceSystem(String endObjSourceSystem) {
        this.endObjSourceSystem = endObjSourceSystem;
    }

    /**
     * @return the endObjCrosswalkValueColumn
     */
    public String getEndObjCrosswalkValueColumn() {
        return endObjCrosswalkValueColumn;
    }

    /**
     * @param endObjCrosswalkValueColumn the endObjCrosswalkValueColumn to set
     */
    public void setEndObjCrosswalkValueColumn(String endObjCrosswalkValueColumn) {
        this.endObjCrosswalkValueColumn = endObjCrosswalkValueColumn;
    }

    /**
     * @return the endObjEntityUriColumn
     */
    public String getEndObjEntityUriColumn() {
        return endObjEntityUriColumn;
    }

    /**
     * @param endObjEntityUriColumn the endObjEntityUriColumn to set
     */
    public void setEndObjEntityUriColumn(String endObjEntityUriColumn) {
        this.endObjEntityUriColumn = endObjEntityUriColumn;
    }

    /**
     * @return the columnDelimiter
     */
    public String getColumnDelimiter() {
        return columnDelimiter;
    }

    /**
     * @param columnDelimiter the columnDelimiter to set
     */
    public void setColumnDelimiter(String columnDelimiter) {
        this.columnDelimiter = columnDelimiter;
    }

    /**
     * @return the isSingleJsonOutput
     */
    public Boolean getIsSingleJsonOutput() {
        return isSingleJsonOutput;
    }

    /**
     * @param isSingleJsonOutput the isSingleJsonOutput to set
     */
    public void setIsSingleJsonOutput(Boolean isSingleJsonOutput) {
        this.isSingleJsonOutput = isSingleJsonOutput;
    }

    /**
     * @return the isJsonWithOutCrosswalk
     */
    public Boolean getIsJsonWithOutCrosswalk() {
        return isJsonWithOutCrosswalk;
    }

    /**
     * @param isJsonWithOutCrosswalk the isJsonWithOutCrosswalk to set
     */
    public void setIsJsonWithOutCrosswalk(Boolean isJsonWithOutCrosswalk) {
        this.isJsonWithOutCrosswalk = isJsonWithOutCrosswalk;
    }

    /**
     * @return the crosswalkValueColumn
     */
    public String getCrosswalkValueColumn() {
        return crosswalkValueColumn;
    }

    /**
     * @param crosswalkValueColumn the crosswalkValueColumn to set
     */
    public void setCrosswalkValueColumn(String crosswalkValueColumn) {
        this.crosswalkValueColumn = crosswalkValueColumn;
    }

    /**
     * @return the startDateColumn
     */
    public String getStartDateColumn() {
        return startDateColumn;
    }

    /**
     * @param startDateColumn the startDateColumn to set
     */
    public void setStartDateColumn(String startDateColumn) {
        this.startDateColumn = startDateColumn;
    }

    /**
     * @return the endDateColumn
     */
    public String getEndDateColumn() {
        return endDateColumn;
    }

    /**
     * @param endDateColumn the endDateColumn to set
     */
    public void setEndDateColumn(String endDateColumn) {
        this.endDateColumn = endDateColumn;
    }

}
