package com.reltio.etl.service;

import static com.reltio.etl.constants.JsonGeneratorProperties.MULTI_VALUE_FLAG;
import static com.reltio.etl.constants.JsonGeneratorProperties.NESTED_ATTR_FLAG;
import static com.reltio.etl.service.util.JSONGeneratorUtil.getSubString;
import static com.reltio.etl.constants.JsonGeneratorCoreProps.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.reltio.etl.util.JsonCreationHelper;
import com.reltio.etl.util.NormalizedJsonConversationHelper;

/**
 * 
 * This class provides required helper methods to generate Entity & Relation
 * JSon Generator Service
 * 
 *
 */
public final class JsonGeneratorService {

	/**
	 * This is the main method which converts the input mapping file to the
	 * Reltio template object with required attributes. Handles all the
	 * attribute types like Simple, Nested and reference with Multi values
	 * 
	 * @param attributeMappingDetails
	 * @param columnIndexMap
	 * @param nonColumnValues
	 * @param fileSourceSystem
	 * @param normalizedColumns
	 * @return Reltio Template Object attributes
	 */
	public static Map<String, Object> createReltioTemplateObjectAttributesList(
			Map<String, String> attributeMappingDetails,
			final Map<String, Integer> columnIndexMap,
			final Set<String> nonColumnValues, String fileSourceSystem,
			final Set<String> normalizedColumns) {

		Map<String, Object> attributesList = new HashMap<>();
		String reltioAttribute = null;
		String inputFileColumn = null;
		String actualInputFileColumn = null;
		boolean isMultiValue;
		for (Entry<String, String> mappingEntry : attributeMappingDetails
				.entrySet()) {
			reltioAttribute = mappingEntry.getKey().trim();
			inputFileColumn = mappingEntry.getValue().trim();
			actualInputFileColumn = inputFileColumn;
			isMultiValue = false;
			if (NormalizedJsonConversationHelper.isMultiValue(inputFileColumn)) {
				actualInputFileColumn = NormalizedJsonConversationHelper
						.getStaticMultiValue(inputFileColumn.trim());
				isMultiValue = true;

			}

			if (columnIndexMap.get(actualInputFileColumn) != null) {
				if (isMultiValue) {
					inputFileColumn = NORMALIZED_VALUE_PREFIX
							+ columnIndexMap.get(actualInputFileColumn)
							+ NORMALIZED_VALUE_SUFFIX;
					normalizedColumns.add(actualInputFileColumn);
				} else {
					inputFileColumn = columnIndexMap.get(actualInputFileColumn)
							+ "";
				}
			} else {
				nonColumnValues.add(inputFileColumn);
			}

			if (reltioAttribute.contains(NESTED_ATTR_FLAG)) {

				createReltioTemplateNestedAttribute(attributesList,
						reltioAttribute, inputFileColumn, fileSourceSystem);

			} else {
				createReltioTemplateSimpleAttribute(attributesList,
						reltioAttribute, inputFileColumn, fileSourceSystem);
			}

		}

		return attributesList;

	}

	public static Map<String, Object> createReltioTemplateInteractionMemberList(
			Map<String, String> membersMappingDetails,
			final Map<String, Integer> columnIndexMap,
			final Set<String> nonColumnValues, String fileSourceSystem,
			final Set<String> normalizedColumns) {

		Map<String, Object> membersList = new HashMap<>();
		String reltioAttribute = null;
		String inputFileColumn = null;
		String actualInputFileColumn = null;
		for (Entry<String, String> mappingEntry : membersMappingDetails
				.entrySet()) {
			reltioAttribute = mappingEntry.getKey().trim();
			inputFileColumn = mappingEntry.getValue().trim();
			actualInputFileColumn = inputFileColumn;

			if (reltioAttribute.contains(INTERACTION_SOURCE_SYSTEM)) {
				createReltioTemplateInteractionMember(membersList,
						reltioAttribute, actualInputFileColumn,
						fileSourceSystem);
			} else if (columnIndexMap.get(actualInputFileColumn) != null) {
				createReltioTemplateInteractionMember(membersList,
						reltioAttribute, columnIndexMap.get(actualInputFileColumn)+"",
						fileSourceSystem);
			} else {
				nonColumnValues.add(inputFileColumn);
			}

		}

		return membersList;

	}

	/**
	 * This is the helper method for creating Reltio template Nested/reference
	 * Attributes from mapping file
	 * 
	 * @param attributesList
	 * @param reltioAttribute
	 * @param inputFileColumn
	 * @param fileSourceSystem
	 */
	@SuppressWarnings("unchecked")
	private static void createReltioTemplateNestedAttribute(
			Map<String, Object> attributesList, String reltioAttribute,
			String inputFileColumn, String fileSourceSystem) {
		String attr = getSubString(0, reltioAttribute, NESTED_ATTR_FLAG);
		String attrPart2 = reltioAttribute.substring(
				reltioAttribute.indexOf(NESTED_ATTR_FLAG) + 1,
				reltioAttribute.length());

		if (attr.contains(MULTI_VALUE_FLAG)) {

			String actualReltioAttribute = getSubString(0, attr,
					MULTI_VALUE_FLAG);
			Integer indexFromInput = Integer.parseInt(attr.substring(
					attr.indexOf(MULTI_VALUE_FLAG) + 1, attr.length()));

			Object nestValObject = attributesList.get(actualReltioAttribute);
			if (nestValObject == null) {
				nestValObject = new ArrayList<Map<String, Object>>();
			}
			List<Map<String, Object>> nestValueList = (List<Map<String, Object>>) nestValObject;
			Map<String, Object> nestValue = null;

			if (nestValueList.size() < indexFromInput) {
				nestValue = new HashMap<>();

			} else {
				nestValue = nestValueList.get(indexFromInput - 1);
			}

			if (!attrPart2.contains(NESTED_ATTR_FLAG)) {
				createReltioTemplateSimpleAttribute(nestValue, attrPart2,
						inputFileColumn, fileSourceSystem);
			} else {
				createReltioTemplateNestedAttribute(nestValue, attrPart2,
						inputFileColumn, fileSourceSystem);
			}
			Map<String, Object> dummyNestValue = null;
			for (int i = nestValueList.size(); i < indexFromInput; i++) {
				dummyNestValue = new HashMap<>();
				nestValueList.add(dummyNestValue);

			}
			nestValueList.set(indexFromInput - 1, nestValue);
			attributesList.put(actualReltioAttribute, nestValueList);

		} else {

			Object nestValObject = attributesList.get(attr);
			if (nestValObject == null) {
				nestValObject = new HashMap<>();
			}
			Map<String, Object> nestedAttribute = (Map<String, Object>) nestValObject;

			if (!attrPart2.contains(NESTED_ATTR_FLAG)) {
				createReltioTemplateSimpleAttribute(nestedAttribute, attrPart2,
						inputFileColumn, fileSourceSystem);
			} else {
				createReltioTemplateNestedAttribute(nestedAttribute, attrPart2,
						inputFileColumn, fileSourceSystem);
			}

			attributesList.put(attr, nestedAttribute);
		}

	}

	@SuppressWarnings("unchecked")
	private static void createReltioTemplateInteractionMember(
			Map<String, Object> membersList, String reltioAttribute,
			String inputFileColumn, String fileSourceSystem) {

		String attr = getSubString(0, reltioAttribute, NESTED_ATTR_FLAG);
		String attrPart2 = reltioAttribute.substring(
				reltioAttribute.indexOf(NESTED_ATTR_FLAG) + 1,
				reltioAttribute.length());

		if (attr.contains(MULTI_VALUE_FLAG)) {

			String actualReltioAttribute = getSubString(0, attr,
					MULTI_VALUE_FLAG);

			Integer indexFromInput = Integer.parseInt(attr.substring(
					attr.indexOf(MULTI_VALUE_FLAG) + 1, attr.length()));

			Object nestValObject = membersList.get(actualReltioAttribute);
			if (nestValObject == null) {
				nestValObject = new ArrayList<Map<String, String>>();
			}
			List<Map<String, String>> nestValueList = (List<Map<String, String>>) nestValObject;
			Map<String, String> nestValue = null;

			if (nestValueList.size() < indexFromInput) {
				nestValue = new HashMap<>();

			} else {
				nestValue = nestValueList.get(indexFromInput - 1);
			}
			nestValue.put(attrPart2, inputFileColumn);

			Map<String, String> dummyNestValue = null;
			for (int i = nestValueList.size(); i < indexFromInput; i++) {
				dummyNestValue = new HashMap<>();
				nestValueList.add(dummyNestValue);

			}
			nestValueList.set(indexFromInput - 1, nestValue);
			membersList.put(actualReltioAttribute, nestValueList);

		} else {

			Object nestValObject = membersList.get(attr);
			if (nestValObject == null) {
				nestValObject = new ArrayList<Map<String, String>>();
			}
			List<Map<String, String>> nestValueList = (List<Map<String, String>>) nestValObject;
			Map<String, String> nestValue = null;

			if (nestValueList.isEmpty()) {
				nestValue = new HashMap<>();
				nestValue.put(attrPart2, inputFileColumn);
				nestValueList.add(nestValue);

			} else {
				nestValue = nestValueList.get(0);
				nestValue.put(attrPart2, inputFileColumn);

				nestValueList.set(0, nestValue);
			}

			membersList.put(attr, nestValueList);
		}

	}

	/**
	 * This is the helper method for Reltio template Simple Attributes from
	 * Mapping File
	 * 
	 * @param attributesList
	 * @param reltioAttribute
	 * @param inputFileColumn
	 * @param fileSourceSystem
	 */
	@SuppressWarnings("unchecked")
	private static void createReltioTemplateSimpleAttribute(
			Map<String, Object> attributesList, String reltioAttribute,
			String inputFileColumn, String fileSourceSystem) {

		if (reltioAttribute.contains(MULTI_VALUE_FLAG)) {
			String actualReltioAttribute = getSubString(0, reltioAttribute,
					MULTI_VALUE_FLAG);
			Object attrValObj = attributesList.get(actualReltioAttribute);
			if (attrValObj == null) {
				attrValObj = new ArrayList<>();
			}

			List<String> attrValues = (List<String>) attrValObj;
			attrValues.add(inputFileColumn);
			attributesList.put(actualReltioAttribute, attrValues);
		} else {
			if (reltioAttribute.equals(REFERENCE_IDENTIFIER)) {
				attributesList.put(reltioAttribute, JsonCreationHelper
						.createCrosswalk(fileSourceSystem, inputFileColumn));
			} else {
				attributesList.put(reltioAttribute, inputFileColumn);
			}
		}

	}

}
