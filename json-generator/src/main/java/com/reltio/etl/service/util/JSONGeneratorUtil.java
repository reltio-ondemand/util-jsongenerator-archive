package com.reltio.etl.service.util;

import com.reltio.etl.constants.JsonGeneratorProperties;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.ReltioObject;
import com.reltio.etl.service.domain.CrosswalkProperty;
import com.reltio.etl.util.JsonConversationHelper;
import com.reltio.etl.util.NormalizedJsonConversationHelper;

import java.util.*;
import java.util.concurrent.Future;

public final class JSONGeneratorUtil {

    public static String getSubString(int begIndex, String strr, String delim) {
        if (strr != null && !strr.isEmpty()) {
            strr = strr.substring(begIndex, strr.indexOf(delim));
        }
        return strr;
    }

    /**
     * Waits for futures (load tasks list put to executor) are partially ready.
     * <code>maxNumberInList</code> parameters specifies how much tasks could be
     * uncompleted.
     *
     * @param futures         - futures to wait for.
     * @param maxNumberInList - maximum number of futures could be left in "undone" state.
     * @return sum of executed futures execution time.
     */
    public static long waitForTasksReady(Collection<Future<Long>> futures,
                                         int maxNumberInList) {
        long totalResult = 0l;
        while (futures.size() > maxNumberInList) {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
                // ignore it...
            }
            for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
                if (future.isDone()) {
                    try {
                        totalResult += future.get();
                        futures.remove(future);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return totalResult;
    }

    public static String[] addValueInArrayBegining(String value, String[] arrayOfValue) {
        List<String> listOfValue = new ArrayList<>(Arrays.asList(arrayOfValue));
        listOfValue.add(0, value);
        return Arrays.copyOf(listOfValue.toArray(), listOfValue.size(), String[].class);
    }


    public static CrosswalkProperty createCrosswalkProperty(String sourceSystem, String crosswalkColumn, String sourceTable) {
        CrosswalkProperty crosswalkProperty = new CrosswalkProperty();

        if (NormalizedJsonConversationHelper.checkNotNull(sourceSystem)) {
            if (NormalizedJsonConversationHelper.isStaticValue(sourceSystem)) {
                crosswalkProperty.setSourceSystem(getStaticSourceSystem(sourceSystem));
                crosswalkProperty.setStaticSourceSystem(true);
            } else if (sourceSystem.startsWith("configuration/sources/")) {
                crosswalkProperty.setSourceSystem(sourceSystem);
                crosswalkProperty.setStaticSourceSystem(true);
            } else {
                crosswalkProperty.setSourceSystem(sourceSystem);
                crosswalkProperty.setStaticSourceSystem(false);
            }
        }

        if (NormalizedJsonConversationHelper.isMultiValue(crosswalkColumn)) {
            crosswalkProperty.setNormalized(true);
            crosswalkProperty.setCrosswalkValueColumn(NormalizedJsonConversationHelper.getStaticMultiValue(crosswalkColumn));
            crosswalkProperty.setSourceSystem(NormalizedJsonConversationHelper.getStaticMultiValue(sourceSystem));

        } else {
            crosswalkProperty.setCrosswalkValueColumn(crosswalkColumn);
        }

        if (NormalizedJsonConversationHelper.checkNotNull(sourceTable)) {
            crosswalkProperty.setSourceTable(sourceTable);
        }

        return crosswalkProperty;
    }

    public static String getStaticSourceSystem(String inputSource) {
        String actualSource = NormalizedJsonConversationHelper.getStaticMultiValue(inputSource).trim();
        if (!actualSource.startsWith("configuration/sources/")) {
            actualSource = "configuration/sources/" + actualSource;
        }

        return actualSource;
    }

    /**
     * This is a utility class to read the JOB configuration properity file for specifically Non Data provider crosswalks
     *
     * @param properties
     * @return
     */
    public static List<CrosswalkProperty> createdNonDPCrosswalks(Properties properties) {

        List<CrosswalkProperty> nonDPCrosswalks = new ArrayList<>();

        if (properties.containsKey(JsonGeneratorProperties.NON_DP_SOURCESYSTEM)) {
            nonDPCrosswalks.add(createCrosswalkProperty(properties.getProperty(JsonGeneratorProperties.NON_DP_SOURCESYSTEM), properties.getProperty(JsonGeneratorProperties.NON_DP_CROSSWALK), properties.getProperty(JsonGeneratorProperties.NON_DP_SOURCE_TABLE)));
        } else {

            for (int i = 1; i < 200; i++) {
                String sourceSystemKey = JsonGeneratorProperties.NON_DP_SOURCESYSTEM.replace(".", "#" + i + ".");
                String crosswalkKey = JsonGeneratorProperties.NON_DP_CROSSWALK.replace(".", "#" + i + ".");
                String sourceTable = JsonGeneratorProperties.NON_DP_SOURCE_TABLE.replace(".", "#" + i + ".");
                if (properties.containsKey(sourceSystemKey)) {
                    nonDPCrosswalks.add(createCrosswalkProperty(properties.getProperty(sourceSystemKey), properties.getProperty(crosswalkKey), properties.getProperty(sourceTable)));
                } else {
                    break;
                }
            }
        }


        return nonDPCrosswalks;
    }

    /**
     * This is the helper class to generate the actual Reltio Crosswalk value for each record. It is generic method which will be used for both Data Provider & Non-DataProvider.
     *
     * @param crosswalkProperty
     * @param crosswalk
     * @param lineValues
     */
    public static void populateReltioDPCrosswalkByFileData(CrosswalkProperty crosswalkProperty, Crosswalk crosswalk, String[] lineValues) {


        if (crosswalkProperty.isStaticSourceSystem()) {
            crosswalk.type = crosswalkProperty.getSourceSystem();
            crosswalk.sourceTable = crosswalkProperty.getSourceTable();
        } else {
            crosswalk.type = lineValues[crosswalkProperty.getSourceSystemColumnIndex()];
            if (!crosswalk.type.startsWith("configuration/sources/")) {
                crosswalk.type = "configuration/sources/" + crosswalk.type;
            }
        }


        crosswalk.value = lineValues[crosswalkProperty.getCrosswalkValueColumnIndex()];

    }

    /**
     * @param crosswalkProperty
     * @param reltioObject
     * @param lineValues
     * @param normalizedColumnDelimiter
     */
    public static void populateReltioNonDPCrosswalkByFileData(CrosswalkProperty crosswalkProperty, ReltioObject reltioObject, String[] lineValues, String normalizedColumnDelimiter) {


        if (crosswalkProperty.isNormalized()) {
            String[] values = NormalizedJsonConversationHelper.getAllValuesFromColumn(lineValues[crosswalkProperty.getCrosswalkValueColumnIndex()], normalizedColumnDelimiter);

            String sourceTable = crosswalkProperty.getSourceTable();
            if (crosswalkProperty.isStaticSourceSystem()) {
                String type = crosswalkProperty.getSourceSystem();
                if (!type.startsWith("configuration/sources/")) {
                    type = "configuration/sources/" + type;
                }
                for (String crosswalkValue : values) {
                    if (JsonConversationHelper.checkNotNull(sourceTable)) {
                        reltioObject.addCrosswalks(type, crosswalkValue, false, sourceTable);
                    } else {
                        reltioObject.addCrosswalks(type, crosswalkValue, false);
                    }
                }
            } else {
                String[] types = NormalizedJsonConversationHelper.getAllValuesFromColumn(lineValues[crosswalkProperty.getSourceSystemColumnIndex()], normalizedColumnDelimiter);
                for (int i = 0; i < values.length; i++) {
                    String srcType = types[i];
                    if (!srcType.startsWith("configuration/sources/")) {
                        srcType = "configuration/sources/" + srcType;
                    }
                    if (JsonConversationHelper.checkNotNull(sourceTable)) {
                        reltioObject.addCrosswalks(srcType, values[i], false, sourceTable);
                    } else {
                        reltioObject.addCrosswalks(srcType, values[i], false);
                    }
                }

            }
        } else {

            String type = null, value = null;

            String sourceTable = crosswalkProperty.getSourceTable();


            if (crosswalkProperty.isStaticSourceSystem()) {
                type = crosswalkProperty.getSourceSystem();
            } else {
                type = lineValues[crosswalkProperty.getSourceSystemColumnIndex()];
                if (!type.startsWith("configuration/sources/")) {
                    type = "configuration/sources/" + type;
                }
            }

            value = lineValues[crosswalkProperty.getCrosswalkValueColumnIndex()];

            if (JsonConversationHelper.checkNotNull(sourceTable)) {
                reltioObject.addCrosswalks(type, value, false, sourceTable);

            } else {
                reltioObject.addCrosswalks(type, value, false);
            }


        }


    }
}
