package com.reltio.etl.service;

import com.google.gson.Gson;
import com.reltio.etl.constants.JsonGeneratorProperties;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.ReltioObject;
import com.reltio.etl.service.domain.CrosswalkProperty;
import com.reltio.etl.service.domain.EntityConfigurationProperties;
import com.reltio.etl.service.util.JSONGeneratorUtil;
import com.reltio.etl.util.NormalizedJsonConversationHelper;
import com.reltio.file.*;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import static com.reltio.etl.constants.JsonGeneratorCoreProps.STATIC_VALUE_PREFIX;
import static com.reltio.etl.constants.JsonGeneratorCoreProps.STATIC_VALUE_SUFFIX;
import static com.reltio.etl.constants.JsonGeneratorProperties.GROUP_TO_SENT;
import static com.reltio.etl.constants.JsonGeneratorProperties.MAX_QUEUE_SIZE_MULTIPLICATOR;
import static com.reltio.etl.service.util.JSONGeneratorUtil.waitForTasksReady;

/**
 * This is the main class for generating json for Entities. Handles both
 * Normalized and denormalized file columns
 */
public class EntityJsonGeneratorService {

    private static Gson gson = new Gson();
    // Keep column name and index in the input file
    private final Map<String, Integer> columnIndexMap = new HashMap<>();
    // Keep non-valid column details list
    private final Set<String> nonColumnValues = new HashSet<>();
    // Keeps Normalized Value columns
    private final Set<String> normalizedColumns = new HashSet<>();
    private final List<ReltioObject> reltioOjectsOfFile = new ArrayList<>();

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void main(String[] args) throws IOException {

        System.out.println("Process Started");
        long programStartTime = System.currentTimeMillis();
        int count = 0;
        final int[] rejectedRecordCount = new int[1];
        rejectedRecordCount[0] = 0;

        final EntityJsonGeneratorService jsonGeneratorService = new EntityJsonGeneratorService();

        Properties properties = new Properties();
        FileReader fileReader = null;
        try {
            String propertyFilePath = args[0];
            fileReader = new FileReader(
                    StringEscapeUtils.escapeJava(propertyFilePath));
            properties.load(fileReader);
        } catch (Exception e) {
            System.out.println("Failed Read the Properties File :: ");
            e.printStackTrace();
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        // READ the Config Properties values
        final EntityConfigurationProperties configProperties = new EntityConfigurationProperties(
                properties);

        if (args != null && args.length > 1) {
            configProperties.setInputDataFilePath(args[1]);
            configProperties.setMappingFilePath(args[2]);
            configProperties.setOutputFilePath(args[3]);
        }

        if (configProperties.getInputDataFilePath() == null
                || configProperties.getEntityType() == null
                || configProperties.getInputFileFormat() == null
                || configProperties.getOutputFilePath() == null
                || configProperties.getMappingFilePath() == null
                || configProperties.getDataProviderCrosswalk() == null
                || configProperties.getDataProviderCrosswalk().getCrosswalkValueColumn() == null
                || configProperties.getDataProviderCrosswalk().getSourceSystem() == null) {

            System.out
                    .println("Error::: one or more required parameters missing. Please verify the input properties File....");
            System.exit(0);
        }

        // Read the Mapping File configuration
        properties.clear();

        try {
            fileReader = new FileReader(
                    StringEscapeUtils.escapeJava(configProperties
                            .getMappingFilePath()));
            properties.load(fileReader);

        } catch (Exception e) {
            System.out.println("Failed to Read the Properties File :: "
                    + configProperties.getMappingFilePath());
            e.printStackTrace();
            System.exit(-1);
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        final Map<String, String> attributeMappingDetails = new HashMap<>(
                (Map) properties);
        // Create Reader for input data File
        ReltioFileReader inputDataFileReader = null;
        final ReltioFileWriter failedRecordsWriter;
        String failedRecordsFileName;

        if (configProperties.getInputFileFormat().equals("CSV")) {
            inputDataFileReader = new ReltioCSVFileReader(
                    configProperties.getInputDataFilePath());
            failedRecordsFileName = configProperties.getOutputFilePath()
                    + "_RejectedRecords.csv";

            failedRecordsWriter = new ReltioCSVFileWriter(failedRecordsFileName);
        } else {
            if (configProperties.getInputFileDelimiter() == null) {
                inputDataFileReader = new ReltioFlatFileReader(
                        configProperties.getInputDataFilePath());
                failedRecordsFileName = configProperties.getOutputFilePath()
                        + "_RejectedRecords.txt";
                failedRecordsWriter = new ReltioFlatFileWriter(
                        failedRecordsFileName);
            } else {
                inputDataFileReader = new ReltioFlatFileReader(
                        configProperties.getInputDataFilePath(),
                        configProperties.getInputFileDelimiter(), "UTF-8");
                failedRecordsFileName = configProperties.getOutputFilePath()
                        + "_RejectedRecords.txt";
                failedRecordsWriter = new ReltioFlatFileWriter(
                        failedRecordsFileName, "UTF-8",
                        configProperties.getInputFileDelimiter());

            }
        }

        // Create Writter for outputFile
        final ReltioFileWriter reltioFileWriter = new ReltioFlatFileWriter(
                configProperties.getOutputFilePath());

        String[] lineValues = null;

        // Read Header
        lineValues = inputDataFileReader.readLine();
        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                .addValueInArrayBegining(
                        JsonGeneratorProperties.FAILED_RECORD_REASON,
                        lineValues));

        final int sizeOfHeaderColumn = lineValues.length;

        List<String> allColumnNames = Arrays.asList(lineValues);

        // Create a Map for Column Name to Index
        String columnName = null;
        for (int i = 0; i < allColumnNames.size(); i++) {

            columnName = allColumnNames.get(i).trim();

            if (NormalizedJsonConversationHelper.isMultiValue(columnName)) {

            }
            jsonGeneratorService.columnIndexMap.put(allColumnNames.get(i)
                    .trim(), i);
        }

        // Populate all the attributes from mapping file to Reltio Dummy Object
        final Map<String, Object> attributesTemplateList = JsonGeneratorService
                .createReltioTemplateObjectAttributesList(
                        attributeMappingDetails,
                        jsonGeneratorService.columnIndexMap,
                        jsonGeneratorService.nonColumnValues,
                        configProperties.getRefAttributeSourceSystem(),
                        jsonGeneratorService.normalizedColumns);

        if ((configProperties.getColumnDelimiter() == null || configProperties
                .getColumnDelimiter().isEmpty())
                && !jsonGeneratorService.normalizedColumns.isEmpty()) {
            System.out
                    .println("Below File columns provided in the mapping File mentioned as Normalized Columns. But the NORMALIZED_FILE_COLUMN_DELIMITER property is empty..");

            for (String column : jsonGeneratorService.normalizedColumns) {
                System.out.println(column);
            }

            System.out
                    .println("Please update the Job Configuration file with the NORMALIZED_FILE_COLUMN_DELIMITER value and Try again...");
            System.exit(-1);

        }

        // Check the crosswalk Column
        jsonGeneratorService.checkDPAndNonDPCrosswalks(configProperties);

        boolean isNonColumnValuePresent = false;

        if (!jsonGeneratorService.nonColumnValues.isEmpty()) {

            for (String col : jsonGeneratorService.nonColumnValues) {
                if (!col.startsWith(STATIC_VALUE_PREFIX)
                        || !col.endsWith(STATIC_VALUE_SUFFIX)) {
                    if (!isNonColumnValuePresent) {
                        System.out
                                .println("Non-Column Values in the Mapping File Below::::");
                    }
                    System.out.println(col);
                    isNonColumnValuePresent = true;
                }
            }
        }

        if (isNonColumnValuePresent) {
            System.out
                    .println("Please validate the Mapping File Column Names listed above and re-try.....");
            System.exit(-1);
        }

        // Thread Operations
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
                .newFixedThreadPool(configProperties.getThreadCount());
        ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
        boolean eof = false;

        // Create Reltio Dummy Object
        final Crosswalk crosswalk = new Crosswalk();
        crosswalk.createDate = configProperties.getCrosswalkCreateDate();
        crosswalk.updateDate = configProperties.getCrosswalkUpdateDate();
        crosswalk.deleteDate = configProperties.getCrosswalkDeleteDate();

        final String normalizedColumnDelimiter = configProperties.getColumnDelimiter();

        List<String[]> inputLineValues = new ArrayList<>();
        while (!eof) {
            for (int threadNum = 0; threadNum < configProperties
                    .getThreadCount() * MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
                inputLineValues.clear();

                for (int k = 0; k < GROUP_TO_SENT; k++) {

                    // Read line
                    lineValues = inputDataFileReader.readLine();
                    if (lineValues == null) {
                        eof = true;
                        break;
                    }
                    count++;
                    inputLineValues.add(lineValues);

                }
                System.out
                        .println("Number of records read from file =" + count);
                if (!inputLineValues.isEmpty()) {
                    final List<String[]> threadInputLines = new ArrayList<>(
                            inputLineValues);

                    futures.add(executorService.submit(new Callable<Long>() {

                        @Override
                        public Long call() {
                            long requestExecutionTime = 0l;
                            long startTime = System.currentTimeMillis();
                            try {
                                ReltioObject reltioObject;
                                Crosswalk reltioObjectCross = null;
                                List<ReltioObject> reltioObjects = null;
                                String[] output;
                                List<String[]> outputLineValues = new ArrayList<>();
                                List<ReltioObject> reltioObjectsOfallRecords = new ArrayList<>();
                                for (String[] lineValues : threadInputLines) {

                                    if (lineValues.length != sizeOfHeaderColumn) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                                                .addValueInArrayBegining(
                                                        " Number of column values not matching with header column count:: Header count: "
                                                                + sizeOfHeaderColumn
                                                                + " .. Current Row column values count: "
                                                                + lineValues.length,
                                                        lineValues));
                                        System.out.println("Rejected: Number of column values not matching with header column count:|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    reltioObject = new ReltioObject();
                                    reltioObjectCross = new Crosswalk();
                                    reltioObjects = new ArrayList<>();

                                    //Add Entity Type to Reltio JSON
                                    reltioObject.type = configProperties
                                            .getEntityType();

                                    //Add Data-Provider Crosswalk to Reltio JSON
                                    JSONGeneratorUtil.populateReltioDPCrosswalkByFileData(configProperties.getDataProviderCrosswalk(), reltioObjectCross, lineValues);


                                    //Add Create/update/Delete dates for Data-provider crosswalk
                                    if (crosswalk.createDate != null
                                            && jsonGeneratorService.columnIndexMap
                                            .get(crosswalk.createDate) != null) {
                                        reltioObjectCross.createDate = lineValues[jsonGeneratorService.columnIndexMap
                                                .get(crosswalk.createDate)];
                                    }

                                    if (crosswalk.updateDate != null
                                            && jsonGeneratorService.columnIndexMap
                                            .get(crosswalk.updateDate) != null) {
                                        reltioObjectCross.updateDate = lineValues[jsonGeneratorService.columnIndexMap
                                                .get(crosswalk.updateDate)];
                                    }

                                    if (crosswalk.deleteDate != null
                                            && jsonGeneratorService.columnIndexMap
                                            .get(crosswalk.deleteDate) != null) {
                                        reltioObjectCross.deleteDate = lineValues[jsonGeneratorService.columnIndexMap
                                                .get(crosswalk.deleteDate)];
                                    }

                                    reltioObject.addCrosswalks(
                                            reltioObjectCross.type,
                                            reltioObjectCross.value.trim(),
                                            reltioObjectCross.createDate,
                                            reltioObjectCross.updateDate,
                                            reltioObjectCross.deleteDate,
                                            reltioObjectCross.sourceTable
                                    );

                                    if (reltioObjectCross.value == null
                                            || reltioObjectCross.value
                                            .isEmpty()) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil.addValueInArrayBegining(
                                                configProperties
                                                        .getDataProviderCrosswalk().getCrosswalkValueColumn()
                                                        + " :: Crosswalk Column Empty",
                                                lineValues));
                                        System.out.println("Rejected: Crosswalk Column Empty|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    if (!configProperties.getNonDPCrosswalks().isEmpty()) {

                                        for (CrosswalkProperty crosswalkProperty : configProperties.getNonDPCrosswalks()) {
                                            //Add NON-Data-Provider Crosswalk to Reltio JSON
                                            JSONGeneratorUtil.populateReltioNonDPCrosswalkByFileData(crosswalkProperty, reltioObject, lineValues, normalizedColumnDelimiter);


                                        }
                                    }

                                    try {
                                        reltioObject
                                                .createAttributes(
                                                        attributesTemplateList,
                                                        lineValues,
                                                        jsonGeneratorService.columnIndexMap,
                                                        jsonGeneratorService.nonColumnValues,
                                                        reltioObjectCross,
                                                        configProperties
                                                                .getColumnDelimiter());

                                        if (configProperties
                                                .getIsSingleJsonOutput()) {
                                            reltioObjectsOfallRecords
                                                    .add(reltioObject);
                                        } else {
                                            reltioObjects.add(reltioObject);
                                            if (configProperties
                                                    .getIsJsonWithOutCrosswalk()) {
                                                output = new String[1];
                                                output[0] = gson
                                                        .toJson(reltioObjects);

                                            } else {
                                                output = new String[2];
                                                output[0] = reltioObjectCross.value;
                                                output[1] = gson
                                                        .toJson(reltioObjects);
                                            }
                                            outputLineValues.add(output);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil.addValueInArrayBegining(
                                                " Generic Error: "
                                                        + e.getMessage(),
                                                lineValues));
                                        System.out.println("Rejected: Failed to do Attribute mapping|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;

                                    }
                                }
                                if (!configProperties.getIsSingleJsonOutput()) {
                                    reltioFileWriter
                                            .writeToFile(outputLineValues);
                                } else {
                                    jsonGeneratorService
                                            .addDataToReltioObjectList(reltioObjectsOfallRecords);
                                }
                                requestExecutionTime = System
                                        .currentTimeMillis() - startTime;
                            } catch (Exception e) {
                                System.out
                                        .println("Unexpected Error happened .... "
                                                + e.getMessage());
                                e.printStackTrace();
                            }
                            return requestExecutionTime;
                        }
                    }));

                }

                if (eof) {
                    break;
                }

            }
            System.out.println("Records processed =" + count);
            waitForTasksReady(futures, configProperties.getThreadCount()
                    * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
        }

        waitForTasksReady(futures, 0);

        if (configProperties.getIsSingleJsonOutput()) {
            reltioFileWriter.writeToFile(gson
                    .toJson(jsonGeneratorService.reltioOjectsOfFile));
        }
        reltioFileWriter.close();
        inputDataFileReader.close();
        failedRecordsWriter.close();
        System.out
                .println("\n \n *** Final Metrics of the JSON Generation ***");

        System.out.println("Total Number of Records in the File = " + count);
        System.out.println("Total Number of Rejected Records = "
                + rejectedRecordCount[0]);
        System.out.println("Total Number of records processed sucessfully = "
                + (count - rejectedRecordCount[0]));
        System.out.println("Total Time Taken in (ms) = "
                + (System.currentTimeMillis() - programStartTime));
        executorService.shutdown();
        if (rejectedRecordCount[0] == 0) {
            new File(failedRecordsFileName).delete();
        }
        System.out.println("Process Completed");
    }

    private synchronized void addDataToReltioObjectList(
            List<ReltioObject> objects) {
        reltioOjectsOfFile.addAll(objects);
    }

    /**
     * This is the helper class to verify the specific crosswalk has the valid columns.
     *
     * @param crosswalkProperty
     */
    private void checkCrosswalkValueColumns(CrosswalkProperty crosswalkProperty) {

        if (columnIndexMap.get(crosswalkProperty.getCrosswalkValueColumn()) == null) {
            nonColumnValues.add(crosswalkProperty.getCrosswalkValueColumn());
        } else {
            crosswalkProperty.setCrosswalkValueColumnIndex(columnIndexMap.get(crosswalkProperty.getCrosswalkValueColumn()));
        }


        if (!crosswalkProperty.isStaticSourceSystem()) {
            if (columnIndexMap.get(crosswalkProperty.getSourceSystem()) == null) {
                nonColumnValues.add(crosswalkProperty.getSourceSystem());
            } else {
                crosswalkProperty.setSourceSystemColumnIndex(columnIndexMap.get(crosswalkProperty.getSourceSystem()));
            }
        }
    }

    /**
     * This is the helper class to verify the File columns are valid or not at the complete Configuration level
     *
     * @param configProperties
     */
    private void checkDPAndNonDPCrosswalks(EntityConfigurationProperties configProperties) {

        //Check the columns in the dataprovider crosswalk;
        checkCrosswalkValueColumns(configProperties.getDataProviderCrosswalk());

        //Check the columns in the NOn dataprovider crosswalks

        for (int i = 0; i < configProperties.getNonDPCrosswalks().size(); i++) {

            CrosswalkProperty crosswalkProperty = configProperties.getNonDPCrosswalks().get(i);
            checkCrosswalkValueColumns(crosswalkProperty);
            configProperties.getNonDPCrosswalks().set(i, crosswalkProperty);

        }
    }
}
