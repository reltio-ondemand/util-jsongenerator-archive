package com.reltio.etl.constants;

public final class JsonGeneratorProperties {

    public static final String NESTED_ATTR_FLAG = ".";
    public static final String REF_ATTR_FLAG = "|";
    public static final String MULTI_VALUE_FLAG = "#";
    public static final Integer MAX_THREAD_COUNT = 30;
    public static final Integer MIN_THREAD_COUNT = 5;
    public static final Integer MAX_QUEUE_SIZE_MULTIPLICATOR = 10;
    public static final Integer GROUP_TO_SENT = 100;
    public static final String FAILED_RECORD_REASON = "Reason for Rejection";
    public static final String SINGLE_JSON_OUTPUT = "SINGLE_JSON";
    public static final String JSON_WITH_CROSSWALK = "JSON_WITH_CROSSWALK_VALUE_COLUMN";
    public static final String JSON_WITHOUT_CROSSWALK = "JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN";
    public static final String NON_DP_CROSSWALK = "NON_DATAPROVIDER.CROSSWALK_VALUE_COLUMN";
    public static final String NON_DP_SOURCESYSTEM = "NON_DATAPROVIDER.SOURCE_SYSTEM";
    public static final String NON_DP_SOURCE_TABLE = "NON_DATAPROVIDER.SOURCE_TABLE";

}
