# Quick Start 
This guide is intended to get you up and running as quickly as possible. For a more detailed view of the utility please visit the [help portal](https://help.reltio.com/index.html#kb/kb_1178.html). 

##Building

The main method of the application varies based on the type of object to be generated. Below is the build path for each type.

1. Entity: **json-generator/src/main/java/com/reltio/etl/service/EntityJsonGeneratorService.java**
2. Interaction: **json-generator/src/main/java/com/reltio/etl/service/InteractionJsonGeneratorService.java**
3. Relation: **json-generator/src/main/java/com/reltio/etl/service/RelationJsonGeneratorService.java**

##Dependencies 

1. gson-2.2.4
2. reltio-cst-core-1.3
3. reltio-cleanse-5.5.jar


##Parameters File Example
The utility is executed with the the path of the parameters file as an argument.
```
#!paintext
INPUT_DATA_FILE_PATH=<<Path of the input source file>>
INPUT_FILE_FORMAT=FLAT/CSV <<Input file format>> It should be either FLAT or CSV
INPUT_FILE_DELIMITER=|| (Only For Flat)
OUTPUT_FILE_PATH=<<Path of the output file that would be generated>>
MAPPING_FILE_PATH=<<Path of the mapping file that is defined>>
RELATION_TYPE=configuration/relationTypes/HasContact <<Relation Type>>
SOURCE_SYSTEM=configuration/sources/Salesforce  <<Source System>>
START_OBJECT_SOURCE_SYSTEM=configuration/sources/Salesforce <<Not mandatory if matches with SOURCE_SYSTEM>>
START_OBJECT_CROSSWALK_VALUE_COLUMN=Email <<Relationship Start Object Crosswalk>>
END_OBJECT_SOURCE_SYSTEM=configuration/sources/Salesforce <<Not mandatory if matches with SOURCE_SYSTEM>>
END_OBJECT_CROSSWALK_VALUE_COLUMN=employer Id <<Relationship End Object Crosswalk>>
THREAD_COUNT=15 (Max is 30. Default is 5) <<No: of threads>>
START_OBJECT_ENTITY_URI_COLUMN=Alias <<If Reltio Start Object in the Source Column is Entity URI>>
END_OBJECT_ENTITY_URI_COLUMN=Username <<If Reltio End Object in the Source Column is Entity URI>>
NORMALIZED_FILE_COLUMN_DELIMITER=$# (Delimiter used for Normalized File Column. This column added to master after grouping the data)
RELATION_CROSSWALK_VALUE_COLUMN=Email<To specify the Relationship crosswalk value if it is present in source file. Optional Field>
JSON_OUTPUT_FORMAT=SINGLE_JSON/JSON_WITH_CROSSWALK_VALUE_COLUMN/JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN

```

##Mapping File Example

```
#!paintext
sfdc_team_mapping.properties
FirstName=First_Name
LastName#1=Last Name
LastName#2=Last Name1
Phone.Number=[Mobilephone]
Phone.Type={Mobile}
Email#1.Type={Work}
Email#1.Email=Role
Email#2.Type={Work}
Email#2.Email#1=Username
Email#2.Email#2=Last Name
Address.AddressLine1=Address1
Address.City=City
Address.refEntity={surrogate}
Employment#1.Name=empname
Employment#1.refEntity=empId
Employment#2.Name=empname2
Employment#2.refEntity=empId2

```

##Executing

Command to start the utility.
```
#!plaintext

java -jar json-generator-entity-2.2.0.jar entity_Config.properties > $logfilepath$

```