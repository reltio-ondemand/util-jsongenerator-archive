package com.reltio.cst.service;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;

/**
 * 
 * This interface provides way to get the token for the given user credentials
 *
 */
public interface TokenGeneratorService {

	public boolean startBackgroundTokenGenerator();

	public String getToken() throws APICallFailureException, GenericException;

	public String getNewToken() throws APICallFailureException,
			GenericException;

	public boolean stopBackgroundTokenGenerator();

}
