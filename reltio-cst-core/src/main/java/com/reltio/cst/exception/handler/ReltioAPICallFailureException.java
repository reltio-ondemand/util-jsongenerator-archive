package com.reltio.cst.exception.handler;

public class ReltioAPICallFailureException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6868629836033459286L;

	private Integer errorCode;
	private String errorResponse;

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(String errorResponse) {
		this.errorResponse = errorResponse;
	}

	public ReltioAPICallFailureException(Integer errorCode, String errorResponse) {

		this.errorCode = errorCode;
		this.errorResponse = errorResponse;
	}

	public ReltioAPICallFailureException(
			APICallFailureException apiCallFailureException) {
		this.errorCode = apiCallFailureException.getErrorCode();
		this.errorResponse = apiCallFailureException.getErrorResponse();

	}

}
