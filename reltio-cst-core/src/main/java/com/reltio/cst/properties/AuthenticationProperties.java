/**
 * 
 */
package com.reltio.cst.properties;

import com.google.gson.Gson;

/**
 *
 *
 */
public final class AuthenticationProperties {

	public static final String AUTH_SERVER_HEADER = "Authorization";

	public static final String AUTH_SERVER_BASIC_TOKEN = "Basic cmVsdGlvX3VpOm1ha2l0YQ==";

	public static final String CONTENT_TYPE_HEADER = "Content-Type";

	public static final String CONTENT_TYPE_JSON = "application/json; charset=UTF-8";

	
	public static final String AUTH_SERVER_API_CALL_TOKEN_PPREFIX = "Bearer ";

	public static final String PASSWORD = "&password=";
	
	public static final String REFRESH_TOKEN = ":refresh_token";

	public static final String DEFAULT_AUTH_SERVER_URL = "https://auth.reltio.com/oauth/token";

	public static final String DEFAULT_AUTH_SERVER_QUERY_PARAM= "?grant_type=password&username=";

	
	public static final String DEFAULT_AUTH_SERVER_REFRESH_TOKEN_QUERY_PARAMS = "?grant_type=refresh_token&refresh_token="
			+ REFRESH_TOKEN;

	public static final Integer RETRY_LIMIT = 4;

	public static final Integer RETRY_LIMIT_FOR_502 = 10;

	public static final Integer RETRY_LIMIT_FOR_503 = 12;

	public static final Integer RETRY_LIMIT_FOR_504 = 5;

	public static final Integer INVALID_USER_CREDENTIALS = 400;
	
	public static final Gson GSON = new Gson();
	
}
