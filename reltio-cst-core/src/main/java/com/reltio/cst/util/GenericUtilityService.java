/**
 * 
 */
package com.reltio.cst.util;

/**
 *
 *
 */
public final class GenericUtilityService {

	public static boolean checkNullOrEmpty(String value) {
		if (value != null && !value.trim().equals("")
				&& !value.trim().equals("UNKNOWN")
				&& !value.trim().equals("<blank>")) {
			return false;
		}
		return true;
	}

	public static boolean checkNull(String value) {
		if (value != null) {
			return false;
		}
		return true;
	}
}
