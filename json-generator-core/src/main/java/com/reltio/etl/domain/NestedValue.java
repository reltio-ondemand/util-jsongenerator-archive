package com.reltio.etl.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * This is the template class for Nested Attributes
 * 
 * 
 */
public class NestedValue {

	public Map<String, Collection<Object>> value = new HashMap<String, Collection<Object>>();

	
}
