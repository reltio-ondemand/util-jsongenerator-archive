package com.reltio.etl.domain;

public class Attribute {

	public Object value;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Attribute) {
			Attribute attr = (Attribute) obj;
			return value.equals(attr.value);
		} else {
			return false;
		}
	}

	public int hashCode() {
		if (value != null)
			return value.hashCode();
		else
			return 0;
	}

}
