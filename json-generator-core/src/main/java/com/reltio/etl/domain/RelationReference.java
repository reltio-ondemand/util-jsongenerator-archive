package com.reltio.etl.domain;

import java.util.ArrayList;
import java.util.List;

public class RelationReference {

	private List<Crosswalk> crosswalks;
	private String objectURI;

	/**
	 * @return the crosswalks
	 */
	public List<Crosswalk> getCrosswalks() {
		if (crosswalks == null) {
			crosswalks = new ArrayList<>();
		}
		return crosswalks;
	}

	/**
	 * @param crosswalks
	 *            the crosswalks to set
	 */
	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}

	/**
	 * @return the objectURI
	 */
	public String getObjectURI() {
		return objectURI;
	}

	/**
	 * @param objectURI
	 *            the objectURI to set
	 */
	public void setObjectURI(String objectURI) {
		this.objectURI = objectURI;
	}

}
