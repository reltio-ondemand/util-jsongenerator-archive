package com.reltio.etl.domain;

import com.reltio.etl.util.JsonCreationHelper;
import com.reltio.etl.util.NormalizedJsonConversationHelper;

import java.util.*;

/**
 * This is the ReltioObject class going to be used Generic Template for all the
 * Entity Types JSON creation
 */
public class ReltioObjectList {

    // Type of the Entity/Relation
    public String type;

    // All List of attributes
    public Map<String, List<Object>> attributes = null;

    // Crosswalks of the Entity/Relation
    private List<Crosswalk> crosswalks;

    //
    private RelationReference startObject;

    private RelationReference endObject;

    // Roles
    private List<String> roles;

    // Tags
    private List<String> tags;

    // Interaction Members
    private Map<String, InteractionMember> members = null;

    // Interaction timestamp
    private String timestamp;

    private String startDate;

    private String endDate;

    private Integer matchScore;


    /**
     * This method will take responsibility of creating members section of
     * Interactions
     *
     * @param memberValues
     * @param lineValues
     * @param columnIndexMap
     * @param nonColumnValues
     * @param crosswalk
     */
    public void createMembers(Map<String, Object> memberValues,
                              String[] lineValues, Map<String, Integer> columnIndexMap,
                              Set<String> nonColumnValues, Crosswalk crosswalk) {

        members = new HashMap<>();
        NormalizedJsonConversationHelper.createMembers(members, memberValues,
                lineValues, columnIndexMap, crosswalk);
        if (members.isEmpty()) {
            members = null;
        }

    }

    /**
     * This is the method will help to add the Crosswalks to Entity with all
     * possible properties inside the Crosswalk
     *
     * @param sourceSystem
     * @param crosswalkValue
     * @param isDataProvider
     * @param createDate
     * @param updateDate
     * @param deleteDate
     */
    public void addCrosswalks(String sourceSystem, String crosswalkValue,
                              Boolean isDataProvider, String createDate, String updateDate,
                              String deleteDate, String sourceTable) {
        if (JsonCreationHelper.checkNull(crosswalkValue)
                && JsonCreationHelper.checkNull(sourceSystem)) {

            Crosswalk crosswalk = JsonCreationHelper.createCrosswalk(
                    sourceSystem, crosswalkValue, isDataProvider, createDate,
                    updateDate, deleteDate, sourceTable);

            if (crosswalks == null) {
                crosswalks = new ArrayList<>();
            }
            crosswalks.add(crosswalk);
        }
    }

    /**
     * This simple helper method to adding crosswalks only with required
     * properties
     *
     * @param sourceSystem
     * @param crosswalkValue
     */
    public void addCrosswalks(String sourceSystem, String crosswalkValue) {
        addCrosswalks(sourceSystem, crosswalkValue, null, null, null, null, null);
    }

    /**
     * This simple helper method to adding crosswalks only with required
     * properties
     *
     * @param sourceSystem
     * @param crosswalkValue
     * @param sourceTable
     */
    public void addCrosswalks(String sourceSystem, String crosswalkValue, String sourceTable) {
        addCrosswalks(sourceSystem, crosswalkValue, null, null, null, null, sourceTable);
    }

    /**
     * This simple helper method to adding crosswalks only with required
     * properties
     *
     * @param sourceSystem
     * @param crosswalkValue
     * @param isDataProvider
     * @param createDate
     * @param updateDate
     */
    public void addCrosswalks(String sourceSystem, String crosswalkValue,
                              Boolean isDataProvider, String createDate, String updateDate) {
        addCrosswalks(sourceSystem, crosswalkValue, isDataProvider, createDate,
                updateDate, null, null);
    }

    /**
     * @param sourceSystem
     * @param crosswalkValue
     * @param createDate
     * @param updateDate
     * @param deleteDate
     */
    public void addCrosswalks(String sourceSystem, String crosswalkValue,
                              String createDate, String updateDate, String deleteDate) {
        addCrosswalks(sourceSystem, crosswalkValue, null, createDate,
                updateDate, deleteDate, null);
    }

    /**
     * This simple helper method to adding crosswalks only with required
     * properties
     *
     * @param sourceSystem
     * @param crosswalkValue
     * @param isDataProvider
     */
    public void addCrosswalks(String sourceSystem, String crosswalkValue,
                              Boolean isDataProvider) {
        addCrosswalks(sourceSystem, crosswalkValue, isDataProvider, null, null,
                null, null);
    }

    /**
     * This method used to add the crosswalk of startobject
     *
     * @param sourceSystem
     * @param crosswalkValue
     */
    public void addRelationStartObjectCrosswalk(String sourceSystem,
                                                String crosswalkValue) {

        if (startObject == null) {
            startObject = new RelationReference();
        }

        startObject.getCrosswalks().add(
                JsonCreationHelper
                        .createCrosswalk(sourceSystem, crosswalkValue));
    }

    /**
     * This method used to add crosswalk of End object
     *
     * @param sourceSystem
     * @param crosswalkValue
     */
    public void addRelationEndObjectCrosswalk(String sourceSystem,
                                              String crosswalkValue) {

        if (endObject == null) {
            endObject = new RelationReference();
        }

        endObject.getCrosswalks().add(
                JsonCreationHelper
                        .createCrosswalk(sourceSystem, crosswalkValue));
    }

    /**
     * This method used to add relationship start object URI
     *
     * @param objectURI
     */
    public void addRelationStartObjectURI(String objectURI) {

        if (startObject == null) {
            startObject = new RelationReference();
        }

        startObject.setObjectURI(objectURI);
    }

    /**
     * This method used to add relationship End object URI
     *
     * @param objectURI
     */
    public void addRelationEndObjectURI(String objectURI) {

        if (endObject == null) {
            endObject = new RelationReference();
        }

        endObject.setObjectURI(objectURI);
    }

    /**
     * @return the roles
     */
    public List<String> getRoles() {
        if (roles == null) {
            roles = new ArrayList<String>();
        }
        return roles;
    }

    /**
     * @return the tags
     */
    public List<String> getTags() {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        return tags;
    }

    /**
     * @return the crosswalks
     */
    public List<Crosswalk> getCrosswalks() {
        return crosswalks;
    }

    /**
     * @return the attributes
     */
    public Map<String, List<Object>> getAttributes() {
        return attributes;
    }

    /**
     * @return the members
     */
    public Map<String, InteractionMember> getMembers() {
        return members;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the matchScore
     */
    public Integer getMatchScore() {
        return matchScore;
    }

    /**
     * @param matchScore the matchScore to set
     */
    public void setMatchScore(Integer matchScore) {
        this.matchScore = matchScore;
    }

}
