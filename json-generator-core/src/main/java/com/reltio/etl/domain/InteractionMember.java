package com.reltio.etl.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InteractionMember implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1069665616424469406L;

	private List<RelationReference> members = null;

	/**
	 * @return the members
	 */
	public List<RelationReference> getMembers() {
		if (members == null) {
			members = new ArrayList<>();

		}
		return members;
	}

	/**
	 * @param members
	 *            the members to set
	 */
	public void setMembers(List<RelationReference> members) {
		this.members = members;
	}

}
