/**
 * 
 */
package com.reltio.etl.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.reltio.etl.domain.Attribute;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.NestedValue;
import com.reltio.etl.domain.ReferenceValue;
import com.reltio.etl.domain.RelationReference;

/**
 * This class is used to generate the JSON for Simple, Nested & Reference
 * Attributes
 * 
 */
public class JsonConversationHelper {

	/**
	 * 
	 * This method is responsible for creating complete attributes from Map to
	 * Reltio
	 * 
	 * @param attributes
	 * @param attributeValues
	 */
	@SuppressWarnings("unchecked")
	public static void createAttributes(
			Map<String, Collection<Object>> attributes,
			Map<String, Object> attributeValues, String[] lineValues,
			Map<String, Integer> columnIndexMap, Set<String> nonColumnValues,
			Crosswalk crosswalk) {

		Set<Object> processAttrs = null;

		if (attributeValues != null) {

			if (attributes == null) {
				attributes = new HashMap<String, Collection<Object>>();
			}

			// Iterate over all the list of attributes
			for (Map.Entry<String, Object> inputValues : attributeValues
					.entrySet()) {

				processAttrs = (Set<Object>) attributes.get(inputValues
						.getKey());

				if (processAttrs == null) {
					processAttrs = new HashSet<>();
				}

				// Check if it is Simple & Single Value Attribute
				if (inputValues.getValue() instanceof String) {
					if (checkNotNull((String) inputValues.getValue())) {

						String inputVal = (String) inputValues.getValue();
						if (isStaticValue(inputVal)) {
							inputVal = getStaticValue(inputVal);
						} else {
							inputVal = getColumnValue(inputVal, lineValues);
						}

						if (checkNotNull(inputVal)) {
							Attribute attribute = new Attribute();
							attribute.value = inputVal;
							processAttrs.add(attribute);
						}
						if (!processAttrs.isEmpty()) {
							attributes.put(inputValues.getKey(), processAttrs);
						}
					}
				}

				// Check if it is multivalued
				if (inputValues.getValue() instanceof List) {
					List<Object> allValues = (List<Object>) inputValues
							.getValue();
					if (!allValues.isEmpty()) {
						Object temp = allValues.get(0);

						// Check if it is Multivalued & Simple
						if (temp instanceof String) {
							List<String> strValues = (List<String>) inputValues
									.getValue();
							for (String inputVal : strValues) {
								if (checkNotNull(inputVal)) {

									if (isStaticValue(inputVal)) {
										inputVal = getStaticValue(inputVal);
									} else {
										inputVal = getColumnValue(inputVal,
												lineValues);
									}

									if (checkNotNull(inputVal)) {
										Attribute attribute = new Attribute();
										attribute.value = inputVal;
										processAttrs.add(attribute);
									}
								}
							}

							if (!processAttrs.isEmpty()) {
								attributes.put(inputValues.getKey(),
										processAttrs);
							}
						} else if (temp instanceof Map) {

							// If it is Multivalued & Nested/reference Attribute
							List<Map<String, Object>> nestedValues = (List<Map<String, Object>>) inputValues
									.getValue();
							for (Map<String, Object> innerNestedValues : nestedValues) {
								handleNestedReferenceValues(innerNestedValues,
										attributes, inputValues.getKey(),
										lineValues, columnIndexMap,
										nonColumnValues, crosswalk);
							}
						}
					}
				} else if (inputValues.getValue() instanceof Map) {
					// If it is Single Valued and Nested/Refernce Attributes
					Map<String, Object> innerNestValues = (Map<String, Object>) inputValues
							.getValue();
					handleNestedReferenceValues(innerNestValues, attributes,
							inputValues.getKey(), lineValues, columnIndexMap,
							nonColumnValues, crosswalk);
				}

			}

		}

	}

	public static boolean isStaticValue(String inputVal) {
		if (inputVal.contains("{") && inputVal.contains("}")) {
			return true;
		}
		return false;
	}

	private static String getColumnValue(String columnIndex, String[] lineValues) {

		String value = lineValues[Integer.parseInt(columnIndex)];

		if (checkNotNull(value)) {
			return value;
		}
		return null;

	}

	private static String getStaticValue(String staticVal) {

		return staticVal.substring(1, staticVal.length() - 1);

	}

	/**
	 * 
	 * This helper method to main Attributes creation method for Nested
	 * Attributes
	 * 
	 * @param attributes
	 * @param nestedAttributeName
	 * @param attributeValues
	 */
	public static void createNestedAttributes(
			Map<String, Collection<Object>> attributes,
			String nestedAttributeName, Map<String, Object> attributeValues,
			String[] lineValues, Map<String, Integer> columnIndexMap,
			Set<String> nonColumnValues, Crosswalk crosswalk) {

		Set<Object> processAttrs = null;
		List<Object> nestedAttrs = null;
		boolean isNonStaticExist = false;

		Map<String, Collection<Object>> innerNestedAttributes = new HashMap<>();
		if (attributes == null) {
			attributes = new HashMap<String, Collection<Object>>();
		}

		if (attributeValues != null && nestedAttributeName != null) {
			nestedAttrs = (List<Object>) attributes.get(nestedAttributeName);

			if (nestedAttrs == null) {
				nestedAttrs = new ArrayList<>();
			}

			for (Map.Entry<String, Object> inputValues : attributeValues
					.entrySet()) {
				processAttrs = new HashSet<>();
				if (inputValues.getValue() instanceof String) {
					if (checkNotNull((String) inputValues.getValue())) {

						String inputVal = (String) inputValues.getValue();
						if (isStaticValue(inputVal)) {
							inputVal = getStaticValue(inputVal);
						} else {
							inputVal = getColumnValue(inputVal, lineValues);
							if (checkNotNull(inputVal)) {
								isNonStaticExist = true;
							}
						}

						if (checkNotNull(inputVal)) {
							Attribute attribute = new Attribute();
							attribute.value = inputVal;
							processAttrs.add(attribute);
						}

						if (!processAttrs.isEmpty()) {
							innerNestedAttributes.put(inputValues.getKey(),
									processAttrs);

						}
					}
				}
				if (inputValues.getValue() instanceof List) {
					@SuppressWarnings("unchecked")
					List<String> allValues = (List<String>) inputValues
							.getValue();
					if (!allValues.isEmpty()) {
						for (String str : allValues) {
							if (checkNotNull(str)) {
								String inputVal = (String) str;
								if (isStaticValue(inputVal)) {
									inputVal = getStaticValue(inputVal);
								} else {
									inputVal = getColumnValue(inputVal,
											lineValues);
									if (checkNotNull(inputVal)) {
										isNonStaticExist = true;
									}
								}

								if (checkNotNull(inputVal)) {
									Attribute attribute = new Attribute();
									attribute.value = inputVal;
									processAttrs.add(attribute);
								}
							}
						}
						if (!processAttrs.isEmpty()) {
							innerNestedAttributes.put(inputValues.getKey(),
									processAttrs);
						}
					}
				} else if (inputValues.getValue() instanceof Map) {
					@SuppressWarnings("unchecked")
					Map<String, Object> innerNestValues = (Map<String, Object>) inputValues
							.getValue();
					createNestedAttributes(innerNestedAttributes,
							inputValues.getKey(), innerNestValues, lineValues,
							columnIndexMap, nonColumnValues, crosswalk);

				}
			}
		}

		if (!innerNestedAttributes.isEmpty() && isNonStaticExist) {
			NestedValue nestedValue = new NestedValue();
			nestedValue.value = innerNestedAttributes;
			nestedAttrs.add(nestedValue);
			attributes.put(nestedAttributeName, nestedAttrs);
		}

	}

	/**
	 * 
	 * This is helper method to main attributes creation method for reference
	 * attributes
	 * 
	 * @param attributes
	 * @param nestedAttributeName
	 * @param attributeValues
	 */
	public static void createReferenceAttributes(
			Map<String, Collection<Object>> attributes,
			String nestedAttributeName, Map<String, Object> attributeValues,
			String[] lineValues, Map<String, Integer> columnIndexMap,
			Set<String> nonColumnValues, Crosswalk crosswalk) {

		Set<Object> processAttrs = null;
		List<Object> refAttrs = null;
		boolean isNonStaticExist = false;

		Map<String, Collection<Object>> innerReferenceAttributes = new HashMap<>();
		if (attributes == null) {
			attributes = new HashMap<String, Collection<Object>>();
		}

		if (attributeValues != null && nestedAttributeName != null) {
			refAttrs = (List<Object>) attributes.get(nestedAttributeName);

			if (refAttrs == null) {
				refAttrs = new ArrayList<>();
			}

			for (Map.Entry<String, Object> inputValues : attributeValues
					.entrySet()) {
				if (!inputValues.getKey().equalsIgnoreCase("refEntity")
						&& !inputValues.getKey()
								.equalsIgnoreCase("refRelation")) {
					processAttrs = new HashSet<>();
					if (inputValues.getValue() instanceof String) {
						String inputVal = (String) inputValues.getValue();

						if (checkNotNull(inputVal)) {

							if (isStaticValue(inputVal)) {
								inputVal = getStaticValue(inputVal);
							} else {
								inputVal = getColumnValue(inputVal, lineValues);
								if (checkNotNull(inputVal)) {
									isNonStaticExist = true;
								}
							}

							if (checkNotNull(inputVal)) {
								Attribute attribute = new Attribute();
								attribute.value = inputVal;
								processAttrs.add(attribute);
							}

							if (!processAttrs.isEmpty()) {
								innerReferenceAttributes.put(
										inputValues.getKey(), processAttrs);
							}
						}

					}
					if (inputValues.getValue() instanceof List) {

						@SuppressWarnings("unchecked")
						List<String> allValues = (List<String>) inputValues
								.getValue();
						if (!allValues.isEmpty()) {
							for (String str : allValues) {
								if (checkNotNull(str)) {
									String inputVal = (String) str;
									if (isStaticValue(inputVal)) {
										inputVal = getStaticValue(inputVal);
									} else {
										inputVal = getColumnValue(inputVal,
												lineValues);
										if (checkNotNull(inputVal)) {
											isNonStaticExist = true;
										}
									}

									if (checkNotNull(inputVal)) {
										Attribute attribute = new Attribute();
										attribute.value = inputVal;
										processAttrs.add(attribute);
									}
								}
							}
							if (!processAttrs.isEmpty()) {
								innerReferenceAttributes.put(
										inputValues.getKey(), processAttrs);
							}
						}
					} else if (inputValues.getValue() instanceof Map) {
						@SuppressWarnings("unchecked")
						Map<String, Object> innerNestValues = (Map<String, Object>) inputValues
								.getValue();
						createNestedAttributes(innerReferenceAttributes,
								inputValues.getKey(), innerNestValues,
								lineValues, columnIndexMap, nonColumnValues,
								crosswalk);

					}
				}
			}
		}

		if (!innerReferenceAttributes.isEmpty() && isNonStaticExist) {
			ReferenceValue referenceValue = new ReferenceValue();
			referenceValue.value = innerReferenceAttributes;
			RelationReference refEntity = new RelationReference();
			RelationReference refRelation = new RelationReference();
			Crosswalk refEntityCross = (Crosswalk) attributeValues
					.get("refEntity");
			Crosswalk refRelationCross = (Crosswalk) attributeValues
					.get("refRelation");
			boolean isRefEntityStatic = false;
			Crosswalk refEntityCrossNew = new Crosswalk();

			if (refEntityCross != null) {
				refEntityCrossNew.type = refEntityCross.type;

				String inputVal = refEntityCross.value;
				if (isStaticValue(inputVal)) {
					inputVal = getStaticValue(inputVal);
					isRefEntityStatic = true;
				} else {
					inputVal = getColumnValue(inputVal, lineValues);
				}
				refEntityCrossNew.value = inputVal;
				refEntity.getCrosswalks().add(refEntityCrossNew);
				referenceValue.refEntity = refEntity;
			}

			refRelationCross = new Crosswalk();
			refRelationCross.type = crosswalk.type;
			refRelationCross.value = "R" + crosswalk.value
					+ nestedAttributeName;
			if (isRefEntityStatic) {
				refRelationCross.value += Math.random();
			} else {
				refRelationCross.value += refEntityCross.value;
			}
			refRelation.getCrosswalks().add(refRelationCross);
			referenceValue.refRelation = refRelation;

			refAttrs.add(referenceValue);
			attributes.put(nestedAttributeName, refAttrs);
		}

	}

	public static Crosswalk createCrosswalk(String sourceSystem,
			String crosswalkValue, Boolean isDataProvider, String createDate,
			String updateDate, String deleteDate) {
		Crosswalk crosswalk = new Crosswalk();
		crosswalk.type = sourceSystem;
		crosswalk.value = crosswalkValue;
		crosswalk.createDate = createDate;
		crosswalk.updateDate = updateDate;
		crosswalk.deleteDate = deleteDate;
		crosswalk.dataProvider = isDataProvider;

		return crosswalk;
	}

	public static Crosswalk createCrosswalk(String sourceSystem,
			String crosswalkValue) {
		Crosswalk crosswalk = new Crosswalk();
		crosswalk.type = sourceSystem;
		crosswalk.value = crosswalkValue;
		return crosswalk;
	}

	// Function to ignore null values
	public static boolean checkNotNull(String value) {
		if (value != null && !value.trim().equals("")
				&& !value.trim().equals("UNKNOWN")
				&& !value.trim().equals("<blank>")
				&& !value.trim().equals("<UNAVAIL>")
				&& !value.trim().equals("#")
				&& !value.toLowerCase().trim().equals("null")
				&& !value.toLowerCase().trim().equals("\"")) {
			return true;
		}
		return false;
	}

	private static void handleNestedReferenceValues(
			Map<String, Object> innerNestValues,
			Map<String, Collection<Object>> attributes, String attributeName,
			String[] lineValues, Map<String, Integer> columnIndexMap,
			Set<String> nonColumnValues, Crosswalk crosswalk) {
		if (innerNestValues.containsKey("refEntity")
				|| innerNestValues.containsKey("refRelation")) {
			createReferenceAttributes(attributes, attributeName,
					innerNestValues, lineValues, columnIndexMap,
					nonColumnValues, crosswalk);
		} else {
			createNestedAttributes(attributes, attributeName, innerNestValues,
					lineValues, columnIndexMap, nonColumnValues, crosswalk);
		}

	}

}
