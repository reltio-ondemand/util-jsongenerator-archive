/**
 * 
 */
package com.reltio.etl.util;

import com.reltio.etl.domain.*;

import java.util.*;

/**
 * This class is used to generate the JSON for Simple, Nested & Reference
 * Attributes
 * 
 */
public class JsonCreationHelper {

	/**
	 * 
	 * This method is responsible for creating complete attributes from Map to
	 * Reltio
	 * 
	 * @param attributes
	 * @param attributeValues
	 */
	@SuppressWarnings("unchecked")
	public static void createAttributes(
			Map<String, Collection<Object>> attributes,
			Map<String, Object> attributeValues) {

		Set<Object> processAttrs = null;

		if (attributeValues != null) {

			if (attributes == null) {
				attributes = new HashMap<String, Collection<Object>>();
			}

			
			//Iterate over all the list of attributes
			for (Map.Entry<String, Object> inputValues : attributeValues
					.entrySet()) {
				
				processAttrs = (Set<Object>) attributes.get(inputValues
						.getKey());

				if (processAttrs == null) {
					processAttrs = new HashSet<>();
				}
				
				// Check if it is Simple & Single Value Attribute
				if (inputValues.getValue() instanceof String) {
					if (checkNull((String) inputValues.getValue())) {

						Attribute attribute = new Attribute();
						attribute.value = inputValues.getValue();

						processAttrs.add(attribute);
						if (!processAttrs.isEmpty()) {
							attributes.put(inputValues.getKey(), processAttrs);
						}
					}
				}
				
				//Check if it is multivalued
				if (inputValues.getValue() instanceof List) {
					List<Object> allValues = (List<Object>) inputValues
							.getValue();
					if (!allValues.isEmpty()) {
						Object temp = allValues.get(0);
						
						//Check if it is Multivalued & Simple
						if (temp instanceof String) {
							List<String> strValues = (List<String>) inputValues
									.getValue();
							for (String str : strValues) {
								if (checkNull(str)) {
									Attribute attribute = new Attribute();
									attribute.value = str;
									processAttrs.add(attribute);

								}
							}

							if (!processAttrs.isEmpty()) {
								attributes.put(inputValues.getKey(),
										processAttrs);
							}
						} else if (temp instanceof Map) {
							
							//If it is Multivalued & Nested/reference Attribute
							List<Map<String, Object>> nestedValues = (List<Map<String, Object>>) inputValues
									.getValue();
							for (Map<String, Object> innerNestedValues : nestedValues) {
								handleNestedReferenceValues(innerNestedValues,
										attributes, inputValues.getKey());
							}
						}
					}
				} else if (inputValues.getValue() instanceof Map) {
					//If it is Single Valued and Nested/Refernce Attributes
					Map<String, Object> innerNestValues = (Map<String, Object>) inputValues
							.getValue();
					handleNestedReferenceValues(innerNestValues, attributes,
							inputValues.getKey());
				}

			}

		}

	}

	/**
	 * 
	 * This helper method to main Attributes creation method for Nested
	 * Attributes
	 * 
	 * @param attributes
	 * @param nestedAttributeName
	 * @param attributeValues
	 */
	public static void createNestedAttributes(
			Map<String, Collection<Object>> attributes,
			String nestedAttributeName, Map<String, Object> attributeValues) {

		Set<Object> processAttrs = null;
		List<Object> nestedAttrs = null;
		Map<String, Collection<Object>> innerNestedAttributes = new HashMap<>();
		if (attributes == null) {
			attributes = new HashMap<String, Collection<Object>>();
		}

		if (attributeValues != null && nestedAttributeName != null) {
			nestedAttrs = (List<Object>) attributes.get(nestedAttributeName);

			if (nestedAttrs == null) {
				nestedAttrs = new ArrayList<>();
			}

			for (Map.Entry<String, Object> inputValues : attributeValues
					.entrySet()) {
				processAttrs = new HashSet<>();
				if (inputValues.getValue() instanceof String) {
					if (checkNull((String) inputValues.getValue())) {
						Attribute attribute = new Attribute();
						attribute.value = inputValues.getValue();
						processAttrs.add(attribute);
						innerNestedAttributes.put(inputValues.getKey(),
								processAttrs);
					}
				}
				if (inputValues.getValue() instanceof List) {
					@SuppressWarnings("unchecked")
					List<String> allValues = (List<String>) inputValues
							.getValue();
					if (!allValues.isEmpty()) {
						for (String str : allValues) {
							if (checkNull(str)) {
								Attribute attribute = new Attribute();
								attribute.value = inputValues
										.getValue();
								processAttrs.add(attribute);

							}
						}
						if (!processAttrs.isEmpty()) {
							innerNestedAttributes.put(inputValues.getKey(),
									processAttrs);
						}
					}
				} else if (inputValues.getValue() instanceof Map) {
					@SuppressWarnings("unchecked")
					Map<String, Object> innerNestValues = (Map<String, Object>) inputValues
							.getValue();
					createNestedAttributes(innerNestedAttributes,
							inputValues.getKey(), innerNestValues);
				}
			}
		}

		if (!innerNestedAttributes.isEmpty()) {
			NestedValue nestedValue = new NestedValue();
			nestedValue.value = innerNestedAttributes;
			nestedAttrs.add(nestedValue);
			attributes.put(nestedAttributeName, nestedAttrs);
		}

	}

	/**
	 * 
	 * This is helper method to main attributes creation method for reference
	 * attributes
	 * 
	 * @param attributes
	 * @param nestedAttributeName
	 * @param attributeValues
	 */
	public static void createReferenceAttributes(
			Map<String, Collection<Object>> attributes,
			String nestedAttributeName, Map<String, Object> attributeValues) {

		Set<Object> processAttrs = null;
		List<Object> refAttrs = null;
		Map<String, Collection<Object>> innerReferenceAttributes = new HashMap<>();
		if (attributes == null) {
			attributes = new HashMap<String, Collection<Object>>();
		}

		if (attributeValues != null && nestedAttributeName != null) {
			refAttrs = (List<Object>) attributes.get(nestedAttributeName);

			if (refAttrs == null) {
				refAttrs = new ArrayList<>();
			}

			for (Map.Entry<String, Object> inputValues : attributeValues
					.entrySet()) {
				if (!inputValues.getKey().equalsIgnoreCase("refEntity")
						&& !inputValues.getKey()
								.equalsIgnoreCase("refRelation")) {
					processAttrs = new HashSet<>();
					if (inputValues.getValue() instanceof String) {
						String inputStr = (String) inputValues.getValue();

						if (checkNull(inputStr)) {
							Attribute attribute = new Attribute();
							attribute.value = inputStr;
							processAttrs.add(attribute);
							innerReferenceAttributes.put(inputValues.getKey(),
									processAttrs);
						}

					}
					if (inputValues.getValue() instanceof List) {
						
						@SuppressWarnings("unchecked")
						List<String> allValues = (List<String>) inputValues
								.getValue();
						if (!allValues.isEmpty()) {
							for (String str : allValues) {
								if (checkNull(str)) {
									Attribute attribute = new Attribute();
									attribute.value = inputValues
											.getValue();
									processAttrs.add(attribute);

								}
							}
							if (!processAttrs.isEmpty()) {
								innerReferenceAttributes.put(
										inputValues.getKey(), processAttrs);
							}
						}
					} else if (inputValues.getValue() instanceof Map) {
						@SuppressWarnings("unchecked")
						Map<String, Object> innerNestValues = (Map<String, Object>) inputValues
								.getValue();
						createNestedAttributes(innerReferenceAttributes,
								inputValues.getKey(), innerNestValues);
					}
				}
			}
		}

		if (!innerReferenceAttributes.isEmpty()) {
			ReferenceValue referenceValue = new ReferenceValue();
			referenceValue.value = innerReferenceAttributes;
			RelationReference refEntity = new RelationReference();
			RelationReference refRelation = new RelationReference();
			Crosswalk refEntityCross = (Crosswalk) attributeValues
					.get("refEntity");
			Crosswalk refRelationCross = (Crosswalk) attributeValues
					.get("refRelation");
			if (refEntityCross != null) {
				refEntity.getCrosswalks().add(refEntityCross);
				referenceValue.refEntity = refEntity;
			}
			if (refRelationCross != null) {
				refRelation.getCrosswalks().add(refRelationCross);
				referenceValue.refRelation = refRelation;
			}
			refAttrs.add(referenceValue);
			attributes.put(nestedAttributeName, refAttrs);
		}

	}

	public static Crosswalk createCrosswalk(String sourceSystem,
											String crosswalkValue, Boolean isDataProvider, String createDate,
											String updateDate, String deleteDate, String sourceTable) {
		Crosswalk crosswalk = new Crosswalk();
		crosswalk.type = sourceSystem;
		crosswalk.value = crosswalkValue;
		crosswalk.createDate = createDate;
		crosswalk.updateDate = updateDate;
		crosswalk.deleteDate = deleteDate;
		crosswalk.dataProvider = isDataProvider;
		crosswalk.sourceTable = sourceTable;

		return crosswalk;
	}

	public static Crosswalk createCrosswalk(String sourceSystem,
			String crosswalkValue) {
		Crosswalk crosswalk = new Crosswalk();
		crosswalk.type = sourceSystem;
		crosswalk.value = crosswalkValue;
		return crosswalk;
	}

	public static Crosswalk createCrosswalk(String sourceSystem,
											String crosswalkValue, String sourceTable) {
		Crosswalk crosswalk = new Crosswalk();
		crosswalk.type = sourceSystem;
		crosswalk.value = crosswalkValue;
		crosswalk.sourceTable = sourceTable;
		return crosswalk;
	}

	// Function to ignore null values
	public static boolean checkNull(String value) {
		return value != null && !value.trim().equals("")
				&& !value.trim().equals("UNKNOWN")
				&& !value.trim().equals("<blank>")
				&& !value.trim().equals("<UNAVAIL>")
				&& !value.trim().equals("#")
				&& !value.toLowerCase().trim().equals("null")
				&& !value.toLowerCase().trim().equals("\"");
	}

	private static void handleNestedReferenceValues(
			Map<String, Object> innerNestValues,
			Map<String, Collection<Object>> attributes, String attributeName) {
		if (innerNestValues.containsKey("refEntity")
				|| innerNestValues.containsKey("refRelation")) {
			createReferenceAttributes(attributes, attributeName,
					innerNestValues);
		} else {
			createNestedAttributes(attributes, attributeName, innerNestValues);
		}

	}

}
