package com.reltio.etl.constants;

public final class JsonGeneratorCoreProps {

	public static final Integer SIZE_OF_RANDOM_STRING = 8;
	public static final String REFERENCE_IDENTIFIER = "refEntity";
	public static final String NORMALIZED_VALUE_PREFIX = "[";
	public static final String NORMALIZED_VALUE_SUFFIX = "]";
	public static final String STATIC_VALUE_PREFIX = "{";
	public static final String STATIC_VALUE_SUFFIX = "}";

	public static final String INTERACTION_CROSSWALK_VALUE = "CROSSWALK_VALUE_COLUMN";
	public static final String INTERACTION_SOURCE_SYSTEM = "SOURCE_SYSTEM";
	public static final String INTERACTION_OBJECT_URI = "OBJECT_URI";

}
