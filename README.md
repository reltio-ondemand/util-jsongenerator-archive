
#JSON Generator Utility

## Description

The Generic JSON Generator is a Java tool developed to convert data stored in a flat or csv format into a JSON format ready to be consumed by the Relio data loader. The tool is exceptionally useful in circumstances where you are attempting to load structured data from from a traditional data platform such a relational database or excel. The tool is capable of loading entities, interactions and relations. 

The application is driven from a configuration file that allows you to specify important information about your source system, data structure, destination, and data mapping. From modifying the configuration file the use will be able to quickly prepare data to be loaded to a Reltio tenant. 

##Change Log


```
#!plaintext

Last Update Date: 06/30/2017
Version: 1.0.0
Description: Initial version
```

##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-jsongenerator/src/b3ab3ab0e80df5f7d71e6b48598264c282716509/QuickStart.md?at=master&fileviewer=file-view-default).


